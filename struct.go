package gisk

type Value struct {
	ValueType ValueType
	Value     interface{}
}
